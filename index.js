const fs   = require('fs')
const convert = require('xml-js')

const filePath = process.argv.slice(2)[0]


// Функция рекурсивно ищет элемент с товарами
async function findOffers (json) {
  if (json.elements) {
    for (const item of json.elements) {
      if (item.name === 'offers' || item.type === 'offers') {
        return item
      }

      const result = await findOffers(item)
      if (result) return result;
    }
  }
}

// Функция парсит товары
async function parseOffers (offers) {
  if (offers) {
    const result = [[], []]

    // Ищем все возможные заголовки для колонок
    offers.forEach(offer => {
      offer.elements.forEach(property => {
        if (!result[0].includes('id')) result[0].push('id')

        if (!result[0].includes(property.name) && property.name !== 'outlets' && property.name !== 'param') {
          result[0].push(property.name)
        }

        if (!result[0].includes('В наличии') && property.name === 'outlets') {
          result[0].push('В наличии')
        }

        if (property.name === 'param' && !result[0].includes(property.attributes.name)) {
          result[0].push(property.attributes.name)
        }
      })
    })

    //Парсим свойства
    offers.forEach(offer => {
      const offerProperties = []
      offerProperties.push({
        text: offer.attributes.id,
        number: result[0].indexOf('id')
      })
      let lastPropertyName = ''
      offer.elements.forEach(property => {
        if (lastPropertyName === 'picture' && property.name === 'picture') {
          offerProperties[offerProperties.length - 1].text += ` ${property.elements[0].text}`
        } else if (property.name === 'outlets') {
          offerProperties.push({
            text: property.elements[0].attributes.instock,
            number: result[0].indexOf('В наличии')
          })
        } else {
          const params = {
            text: 'none',
            number: result[0].indexOf(property.name)
          }
          if (property.name === 'param') {
            params.number = result[0].indexOf(property.attributes.name)
          }
          if (property.elements && property.elements[0].text) {
            params.text = property.elements[0].text
          } else if (property.elements && property.elements[0].cdata) {
            params.text = property.elements[0].cdata
          }
          offerProperties.push(params)
        }
        lastPropertyName = property.name
      })
      result[1].push(offerProperties)
    })

    //Сортируем все свойства товаров в соответствии с порядком заголовков
    result[1].forEach(item => {
      let n = item.length;
      for (let i = 0; i < n-1; i++){ // Выполняется для каждого элемента массива, кроме последнего.
        for (let j = 0; j < n-1-i; j++){ // Для всех последующих за текущим элементов
          if (item[j+1].number < item[j].number){ // выпоняется проверка, и если следующий элемент меньше текущего
            let t = item[j+1]; item[j+1] = item[j]; item[j] = t; // то эти элементы меняются местами.
          }
        }
      }
    })

    //Убираем лишние поля у свойств и заполняем промежуточные пустые свойства
    const newArr = []
    result[1].forEach((element, i) => {
      newArr.push([])
      let lastPropNumber = 0
      element.forEach(property => {
        if (property.number - lastPropNumber > 1) {
          for (let diff = 0; diff < property.number - lastPropNumber - 1; diff++ ) {
            newArr[i].push('none')
          }
        }
        lastPropNumber = property.number
        newArr[i].push(property.text)
      })
    })
    result[1] = newArr

    const newRes = [
      result[0],
      ...result[1]
    ]
    return newRes.map(e => e.join(";")).join("\n")
  }
}

(async function () {
  try {
    console.log('Считываем файл')
    const doc = fs.readFileSync(filePath, 'utf8')
    const parsed = convert.xml2js(doc, {
      compact: false,
      spaces: 2,
      ignoreInstruction: true,
      ignoreDeclaration: true,
      ignoreDoctype: true
    })

    console.log('Ищем товары')
    const offers = await findOffers(parsed)
    // console.log(offers)

    console.log('Достаем свойства товаров')
    const parsedOffers = await parseOffers(offers.elements)
    // console.log(parsedOffers)
    console.log('Сохраняем готовый файл')
    fs.writeFile('./out.csv', parsedOffers, 'utf8', function (err) {
      if (err) {
        console.log('Какая-то ошибка, файл не сохранен');
      } else{
        console.log('Готово!');
      }
    })
  } catch (e) {
    console.error(e)
  }
}())
